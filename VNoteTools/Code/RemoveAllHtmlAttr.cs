﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace VNoteTools.Code
{
    internal class RemoveAllHtmlAttr
    {
        internal static string RemoveAllAttributes(string htmlText)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlText);

            // 遍历所有标签
            foreach (HtmlNode node in doc.DocumentNode.DescendantsAndSelf())
            {
                // 移除当前标签的所有属性
                while (node.Attributes.Count > 0)
                {
                    node.Attributes.Remove(node.Attributes[0]);
                }
            }

            // 返回处理后的HTML文本
            return doc.DocumentNode.OuterHtml;
        }

        internal static void RemoveAllAttr(string htmlFilePath, string modifiedHtmlFilePath, Encoding encoding = null)

        {
            if (encoding == null)
                encoding = Encoding.UTF8;
            // 读取HTML文件内容          
            string htmlText = System.IO.File.ReadAllText(htmlFilePath, encoding);

            // 移除所有标签内的属性
            string modifiedHtml = RemoveAllAttributes(htmlText);

            // 将处理后的HTML保存到新文件            
            System.IO.File.WriteAllText(modifiedHtmlFilePath, modifiedHtml, encoding);

            Console.WriteLine("HTML属性已移除并保存到新文件： " + modifiedHtmlFilePath);
        }
    }
}
